/**
 * Datepicker i18n module for english language (en_us)
 * @author: Anton Ivanov <a.ivanov@aladdin-rd.ru>
 * @copyright: Anton Ivanov
 * @date: 18.09.2012
 * @version: 0.1-alpha
 */

$.fn.datepicker.i18n = {
    days: {
        full:    ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
        short:   ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
        minimum: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"]
    },
    months: {
        full:    ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        short:   ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    },
    labels: {
        today:   "Today"
    }
};