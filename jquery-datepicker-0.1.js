/**
 * Datepicker jQuery plugin.
 * Based on bootstrap-datepicker.js by Stefan Petre.
 *
 * @author: Anton Ivanov <a.ivanov@aladdin-rd.ru>
 * @copyright: Anton Ivanov
 * @date: 18.09.2012
 * @version: 0.1-alpha
 */

(function($){
    /* Datepicker object storage */
    var _dp;

    /* Datepicker settings */
    var _settings;

    /* Parsed date format */
    var _format;

    /* Calendar date object */
    var _date;

    /* Form control triggered plug-in */
    var _target;

    /* View mode: days, months, years */
    var _mode;

    /* Datepicker internationalization object */
    var i18n;

    /* Datepicker plugin public methods */
    var methods = {
        'init': function(options){

            // Joining default options with user provider
            _settings = $.extend({
                // Date format
                'format'       : 'dd.mm.yyyy',
                // First day of the week: 0 - Sunday, 1 - Monday
                'weekStart'    : 1,
                // Hide datepicker after date was selected
                'hideOnSelect' : true,
                // Default display mode: days, months, years
                'defaultMode'  : 'days',
                // Class name
                'class'        : false,
                // DOM Element to append to
                'parent'       : 'body',
                // Show 'Today' link
                'showToday'    : true
            }, options);

            // Getting internationalization object
            i18n = $.fn.datepicker.i18n;

            return this.each(function () {
                // Parse date format
                _format = methods.parseFormat(_settings.format);

                // Applying template and event handlers to
                // Datepicker object
                if (typeof _dp == 'undefined') {
                    _dp = $(template).appendTo(_settings.parent).on({
                        'click'     : $.proxy(methods.onclick, methods),
                        'mousedown' : $.proxy(methods.onmousedown, methods)
                    });

                    if (_settings.class) {
                        _dp.addClass(_settings.class);
                    }

                    if (_settings.showToday) {
                        $('<span class="datepicker-footer"><span class="today"><i class="icon-calendar"/> ' + i18n.labels.today + '</span></span>').appendTo(_dp);
                    }
                }

                // Applying event handlers to target element which triggered
                // plugin (usually an input field)
                $(this).on({
                    'focus' : $.proxy(methods.show, methods),
                    'blur'  : $.proxy(methods.hide, methods)
                });
            });
        },
        'show': function(event){
            event.stopPropagation();
            event.preventDefault();

            // Set target element
            _target = $(event.target);

            // Set display mode
            _mode = _settings.defaultMode;

            this.position(_target);
            this.update(_target.val());
            _dp.show();
        },
        'hide': function(){
            _dp.hide();
        },
        'position': function(target){
            var offset = target.offset();
            _dp.css({
                top  : Math.round(offset.top + target.outerHeight()),
                left : Math.round(offset.left)
            });
        },
        'update': function(date){
            _date = date ? this.parseDate(date, _format) : _date;
            methods['fill' + _mode.charAt(0).toUpperCase() + _mode.slice(1)].call(this, _date);
            this.switchTo(_mode);
        },
        'fillWeekDays': function(){
            var html = '';
            for (var counter = _settings.weekStart; counter < _settings.weekStart + 7; counter++) {
                html += '<th>' + i18n.days.minimum[counter % 7] + '</th>';
            }
            _dp.find('.datepicker-days .week-days').remove();
            _dp.find('.datepicker-days thead').append('<tr class="week-days">' + html + '</tr>');
        },
        'fillDays': function(date){
            var year      = date.getFullYear(),
                month     = date.getMonth(),
                weekStart = _settings.weekStart,
                weekEnd   = weekStart == 0 ? 6 : weekStart - 1,
                curDate   = new Date(),
                html      = [],
                cls       = [];

            this.fillWeekDays();

            _dp.find('.datepicker-days th:eq(1)').text(i18n.months.full[month] + ' ' + year).data('switch', 'months');

            curDate.setHours(0, 0, 0, 0);

            var monthPrev    = new Date(year, month - 1, 28, 0, 0, 0, 0),
                daysPerMonth = this.getDaysInMonth(monthPrev.getFullYear(), monthPrev.getMonth());
            monthPrev.setDate(daysPerMonth);
            monthPrev.setDate(daysPerMonth - (monthPrev.getDay() - weekStart + 7) % 7);

            var monthNext = new Date(monthPrev);
            monthNext.setDate(monthNext.getDate() + 42);

            while (monthPrev.valueOf() < monthNext.valueOf()) {
                cls = [];
                if (monthPrev.getDay() == weekStart) {
                    html.push('<tr>');
                }

                if (monthPrev.getMonth() == month) {
                    cls.push('in-month');
                }

                if (monthPrev.valueOf() == curDate.valueOf()) {
                    cls.push('active');
                }

                html.push('<td class="' + cls.join(' ') + '">' + monthPrev.getDate() + '</td>');

                if (monthPrev.getDay() == weekEnd) {
                    html.push('</tr>');
                }

                monthPrev.setDate(monthPrev.getDate() + 1);
            }

            _dp.find('.datepicker-days tbody').empty().append(html.join(''));
        },
        'fillMonths': function(date){
            var year      = date.getFullYear(),
                curDate   = new Date(),
                html      = [],
                cls       = '';

            for (var counter = 0; counter < 12; counter++) {
                if (counter % 4 == 0) {
                    html.push('<tr>');
                }

                cls = (counter == curDate.getMonth() && year == curDate.getFullYear()) ? ' class="active"' : '';
                html.push('<td' + cls + '>' + i18n.months.short[counter] + '</td>');

                if (counter % 4 == 3) {
                    html.push('</tr>');
                }
            }
            _dp.find('.datepicker-months th:eq(1)').text(year).data('switch', 'years');
            _dp.find('.datepicker-months td').empty().append('<table>' + html.join('') + '</table>');
        },
        'fillYears': function(date){
            var year      = date.getFullYear() - 5,
                curDate   = new Date(),
                html      = [],
                cls       = '';

            for (var counter = 0; counter < 12; counter++) {
                if (counter % 4 == 0) {
                    html.push('<tr>');
                }

                cls = ((year + counter) == curDate.getFullYear()) ? ' class="active"' : '';
                html.push('<td' + cls + '>' + (year + counter) + '</td>');

                if (counter % 4 == 3) {
                    html.push('</tr>');
                }
            }
            _dp.find('.datepicker-years th:eq(1)').html(year + ' &ndash; ' + (year + 11));
            _dp.find('.datepicker-years td').empty().append('<table>' + html.join('') + '</table>');
        },
        'parseFormat': function(format){
            var separator = format.match(/[.\/-].*?/),
                parts     = format.split(/\W+/);
            if (!separator || !parts || parts.length == 0){
                throw new Error("datepicker.js: invalid date format");
            }
            return {separator: separator, parts: parts};
        },
        'parseDate': function(strDate, format){
            var parts = strDate.split(format.separator),
                date  = new Date(),
                val   = null;

            if (parts.length == format.parts.length) {
                for (var i = 0; i < format.parts.length; i++) {
                    val = parseInt(parts[i], 10) || 0;
                    switch(format.parts[i]) {
                        case 'dd':
                        case 'd':
                            date.setDate(val);
                            break;
                        case 'mm':
                        case 'm':
                            date.setMonth(val - 1);
                            break;
                        case 'yy':
                            date.setFullYear(2000 + val);
                            break;
                        case 'yyyy':
                            date.setFullYear(val);
                            break;
                    }
                    date.setHours(0, 0, 0, 0);
                }
            }

            return date;
        },
        'formatDate': function(date, format){
            var value = {
                d:    date.d,
                dd:   (date.d < 10 ? '0' : '') + date.d,
                m:    date.m + 1,
                mm:   (date.m < 10 ? '0' : '') + (date.m + 1),
                yy:   date.y.toString().substring(2),
                yyyy: date.y
                },
                fdate = [];

            for (var i = 0; i < format.parts.length; i++) {
                fdate.push(value[format.parts[i]]);
            }

            return fdate.join(format.separator);
        },
        'setInputValue': function(date) {
            _target.prop('value', this.formatDate(date, _format));
            if (_settings.hideOnSelect) {
                this.hide();
            }
        },
        'isLeapYear': function (year) {
            return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0))
        },
        'getDaysInMonth': function (year, month) {
            return [31, (this.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month]
        },
        'switchTo': function(mode) {
            _mode = mode;
            _dp.find('>div').hide().filter('.datepicker-' + (mode || 'days')).show();
        },
        'onclick': function(event) {
            event.stopPropagation();
            event.preventDefault();

            var target = $(event.target).closest('span, td, th');
            if (target.length == 1) {
                switch(target[0].nodeName.toLowerCase()) {
                    case 'th':
                        switch(target[0].className) {
                            case 'switch':
                                var to = target.data('switch');
                                if (to) {
                                    this.switchTo(to);
                                    this.update();
                                }
                                break;
                            case 'prev':
                            case 'next':
                                var step = (_mode == 'days') ? 1 : 12;
                                if (_mode == 'years') {
                                    step = step * 12;
                                }
                                _date = new Date(_date.getFullYear(), _date.getMonth() + (target[0].className == 'next' ? step : -step), 1);
                                this.update();
                                break;
                        }
                        break;
                    case 'td':
                        switch (_mode) {
                            case 'months':
                                _date = new Date((target.closest('table').find('td').index(target) + 1) + '/01/' + _date.getFullYear());
                                this.switchTo('days');
                                this.update();
                                break;
                            case 'years':
                                _date = new Date('01/01/' + target.text());
                                this.switchTo('months');
                                this.update();
                                break;
                            default:
                                if (!target.is('.in-month')) {
                                    return false;
                                }

                                var date = {
                                    d: parseInt(target.text(), 10) || 1,
                                    m: _date.getMonth(),
                                    y: _date.getFullYear()
                                };

                                this.setInputValue(date);
                                break;
                        }
                        break;
                    case 'span':
                        if (target[0].className == 'today') {
                            _date = new Date();
                            _date.setHours(0, 0, 0, 0);
                            this.switchTo('days');
                            this.update();
                        }
                        break;
                }
            }
        },
        'onmousedown': function(event){
            event.stopPropagation();
            event.preventDefault();
        }
    };

    var _templates = {
        head:'<thead><tr><th class="prev"><i class="icon-arrow-left"/></th><th colspan="5" class="switch"></th><th class="next"><i class="icon-arrow-right"/></th></tr></thead>',
        body:'<tbody><tr><td colspan="7"></td></tr></tbody>'
    };

    var template = '<div class="datepicker"><div class="datepicker-days"><table>'+_templates.head+'<tbody></tbody>'+'</table></div><div class="datepicker-months"><table>'+_templates.head+_templates.body+'</table></div><div class="datepicker-years"><table>'+_templates.head+_templates.body+'</table></div></div>';

    $.fn.datepicker = function(method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.datepicker');
            return false;
        }
    };

})(jQuery);